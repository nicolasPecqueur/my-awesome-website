package fr.nico.myawesomewebsite.persistance;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.nico.myawesomewebsite.domaine.entities.Gabarit;

/**
 * The Interface GabaritRepository.
 */
@Repository
public interface GabaritRepository extends JpaRepository<Gabarit, Long> {

	/**
	 * Find by user.
	 *
	 * @param id the id
	 * @return the optional
	 */
	@Query("select gbt from Utilisateur uti "
			+ "left join uti.gabarit gbt "
			+ "where uti.id = :id")
	Optional<Gabarit> findByUser(@Param("id") Integer id);
}
