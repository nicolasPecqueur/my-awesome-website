package fr.nico.myawesomewebsite.persistance;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.nico.myawesomewebsite.domaine.entities.Tag;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    Optional<Tag> findByLibelle(String tag);
    
    @Query("select t from Tag t "
            + "where t.nbAttributions > 0 "
            + "and t.libelle not in ('en-cours') "
            + "order by t.libelle asc ")
    List<Tag> findForMurTags();
}
