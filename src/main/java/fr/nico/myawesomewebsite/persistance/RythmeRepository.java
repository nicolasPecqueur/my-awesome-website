package fr.nico.myawesomewebsite.persistance;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.nico.myawesomewebsite.domaine.entities.Rythme;

/**
 * The Interface RythmeRepository.
 */
@Repository
public interface RythmeRepository extends JpaRepository<Rythme, Long> {

	/**
	 * Gets the rythme by user id.
	 *
	 * @param id the id
	 * @return the rythme by user id
	 */
	@Query("select rtm from Utilisateur uti "
			+ "left join uti.rythme rtm "
			+ "where uti.id = :id")
	Optional<Rythme> getRythmeByUserId(@Param("id") Integer id);
}
