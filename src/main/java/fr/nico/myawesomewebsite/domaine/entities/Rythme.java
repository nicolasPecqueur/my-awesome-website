package fr.nico.myawesomewebsite.domaine.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.nico.myawesomewebsite.domaine.referentiel.RythmeEntrainementHebdomadaireEnum;

/**
 * The Class Rythme d'entrainement hebdomadaire et d'alimentation journalier.
 */
@Entity
@Table(name = "RYTHME_ENTRAINEMENT_ALIMENTAIRE")
public class Rythme {
	
	/** The id. */
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	/** The rythme entrainement hebdomadaire. */
	@Column(name = "ENTRAINEMENT_HEBDOMADAIRE")
	private RythmeEntrainementHebdomadaireEnum entrainementHebdomadaire;
	
	/** The nb repas jour. */
	@Column(name = "NOMBRE_REPAS_JOUR")
	private int nbRepasJour;
	
	/** The proteine kilo. */
	@Column(name = "PROTEINE_PAR_KILO")
	private double proteineKilo;
	
	/** The lipide kilo. */
	@Column(name = "LIPIDE_PAR_KILO")
	private double lipideKilo;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the entrainement hebdomadaire.
	 *
	 * @return the entrainement hebdomadaire
	 */
	public RythmeEntrainementHebdomadaireEnum getEntrainementHebdomadaire() {
		return entrainementHebdomadaire;
	}

	/**
	 * Sets the entrainement hebdomadaire.
	 *
	 * @param entrainementHebdomadaire the new entrainement hebdomadaire
	 */
	public void setEntrainementHebdomadaire(RythmeEntrainementHebdomadaireEnum entrainementHebdomadaire) {
		this.entrainementHebdomadaire = entrainementHebdomadaire;
	}

	/**
	 * Gets the nb repas jour.
	 *
	 * @return the nb repas jour
	 */
	public int getNbRepasJour() {
		return nbRepasJour;
	}

	/**
	 * Sets the nb repas jour.
	 *
	 * @param nbRepasJour the new nb repas jour
	 */
	public void setNbRepasJour(int nbRepasJour) {
		this.nbRepasJour = nbRepasJour;
	}

	/**
	 * Gets the proteine kilo.
	 *
	 * @return the proteine kilo
	 */
	public double getProteineKilo() {
		return proteineKilo;
	}

	/**
	 * Sets the proteine kilo.
	 *
	 * @param proteineKilo the new proteine kilo
	 */
	public void setProteineKilo(double proteineKilo) {
		this.proteineKilo = proteineKilo;
	}

	/**
	 * Gets the lipide kilo.
	 *
	 * @return the lipide kilo
	 */
	public double getLipideKilo() {
		return lipideKilo;
	}

	/**
	 * Sets the lipide kilo.
	 *
	 * @param lipideKilo the new lipide kilo
	 */
	public void setLipideKilo(double lipideKilo) {
		this.lipideKilo = lipideKilo;
	}
	
}
