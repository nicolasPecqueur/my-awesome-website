package fr.nico.myawesomewebsite.domaine.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.nico.myawesomewebsite.domaine.referentiel.TypeMetabolismeEnum;

/**
 * The Class Metabolisme.
 */
@Entity
@Table(name = "METABOLISME")
public class Metabolisme {
    
    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    
    /** The valeur. */
    @Column(name = "VALEUR")
    private int valeur;
    
    /** The type. */
    @Column(name = "TYPE_MET")
    private TypeMetabolismeEnum type;
    
    /** The proteines repas. */
    @Column(name = "PROTEINES_PAR_REPAS")
    private double proteinesRepas;
    
    /** The lipides repas. */
    @Column(name = "LIPIDES_PAR_REPAS")
    private double lipidesRepas;
    
    /** The glucides repas. */
    @Column(name = "GLUCIDES_PAR_REPAS")
    private double glucidesRepas;

    public Metabolisme(int valeur, TypeMetabolismeEnum type, double proteinesRepas, double lipidesRepas, double glucidesRepas) {
        
        this.valeur = valeur;
        this.type = type;
        this.proteinesRepas = proteinesRepas;
        this.lipidesRepas = lipidesRepas;
        this.glucidesRepas = glucidesRepas;
    }

    /**
     * Constructeur par défaut.
     */
    public Metabolisme() {
        this(0, TypeMetabolismeEnum.BASAL, 0.0, 0.0, 0.0);
    }

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the valeur.
	 *
	 * @return the valeur
	 */
	public int getValeur() {
		return valeur;
	}

	/**
	 * Sets the valeur.
	 *
	 * @param valeur the new valeur
	 */
	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public TypeMetabolismeEnum getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(TypeMetabolismeEnum type) {
		this.type = type;
	}

	/**
	 * Gets the proteines repas.
	 *
	 * @return the proteines repas
	 */
	public double getProteinesRepas() {
		return proteinesRepas;
	}

	/**
	 * Sets the proteines repas.
	 *
	 * @param proteinesRepas the new proteines repas
	 */
	public void setProteinesRepas(double proteinesRepas) {
		this.proteinesRepas = proteinesRepas;
	}

	/**
	 * Gets the lipides repas.
	 *
	 * @return the lipides repas
	 */
	public double getLipidesRepas() {
		return lipidesRepas;
	}

	/**
	 * Sets the lipides repas.
	 *
	 * @param lipidesRepas the new lipides repas
	 */
	public void setLipidesRepas(double lipidesRepas) {
		this.lipidesRepas = lipidesRepas;
	}

	/**
	 * Gets the glucides repas.
	 *
	 * @return the glucides repas
	 */
	public double getGlucidesRepas() {
		return glucidesRepas;
	}

	/**
	 * Sets the glucides repas.
	 *
	 * @param glucidesRepas the new glucides repas
	 */
	public void setGlucidesRepas(double glucidesRepas) {
		this.glucidesRepas = glucidesRepas;
	}

}
