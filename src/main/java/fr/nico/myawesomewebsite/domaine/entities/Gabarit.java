package fr.nico.myawesomewebsite.domaine.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Gabarit.
 */
@Entity
@Table(name = "GABARIT")
public class Gabarit {

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    
    /** The poids. */
    
    @Column(name = "POIDS")
    private double poids;
    
    /** The taille. */
    @Column(name = "TAILLE")
    private int taille;
    
    /** The tour de taille. */
    @Column(name = "TOUR_DE_TAILLE")
    private double tourDeTaille;
    
    /** The tour de cou. */
    @Column(name = "TOUR_DE_COU")
    private double tourDeCou;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the poids.
     *
     * @return the poids
     */
    public double getPoids() {
        return poids;
    }

    /**
     * Sets the poids.
     *
     * @param poids the new poids
     */
    public void setPoids(double poids) {
        this.poids = poids;
    }

    /**
     * Gets the taille.
     *
     * @return the taille
     */
    public int getTaille() {
        return taille;
    }

    /**
     * Sets the taille.
     *
     * @param taille the new taille
     */
    public void setTaille(int taille) {
        this.taille = taille;
    }

    /**
     * Gets the tour de taille.
     *
     * @return the tour de taille
     */
    public double getTourDeTaille() {
        return tourDeTaille;
    }

    /**
     * Sets the tour de taille.
     *
     * @param tourDeTaille the new tour de taille
     */
    public void setTourDeTaille(double tourDeTaille) {
        this.tourDeTaille = tourDeTaille;
    }

    /**
     * Gets the tour de cou.
     *
     * @return the tour de cou
     */
    public double getTourDeCou() {
        return tourDeCou;
    }

    /**
     * Sets the tour de cou.
     *
     * @param tourDeCou the new tour de cou
     */
    public void setTourDeCou(double tourDeCou) {
        this.tourDeCou = tourDeCou;
    }

}
