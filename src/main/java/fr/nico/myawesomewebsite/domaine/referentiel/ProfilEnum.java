package fr.nico.myawesomewebsite.domaine.referentiel;

/**
 * The Enum Profil.
 */
public enum ProfilEnum {

    /** The user. */
    USER("Utilisateur"),

    /** The admin. */
    ADMIN("Administrateur");

    /** The nom profil. */
    private final String nomProfil;

    /**
     * Instantiates a new profil enum.
     *
     * @param nomProfil the nom profil
     */
    private ProfilEnum(String nomProfil) {
        this.nomProfil = nomProfil;
    }

    /**
     * Gets the nom profil.
     *
     * @return the nom profil
     */
    public String getNomProfil() {
        return this.nomProfil;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
        return this.nomProfil;
    }

}
