package fr.nico.myawesomewebsite.webapp.dto.fil;

public class FilToUpdateDto extends FilToSaveDto {

    private OrigineDto origine;

    private boolean supprimerImageActuelle = false;

    private String cheminImageActuelle;

    public OrigineDto getOrigine() {
        return this.origine;
    }

    public void setOrigine(OrigineDto origine) {
        this.origine = origine;
    }

    public boolean isSupprimerImageActuelle() {
        return this.supprimerImageActuelle;
    }

    public void setSupprimerImageActuelle(boolean supprimerImageActuelle) {
        this.supprimerImageActuelle = supprimerImageActuelle;
    }

    public String getCheminImageActuelle() {
        return this.cheminImageActuelle;
    }

    public void setCheminImageActuelle(String cheminImageActuelle) {
        this.cheminImageActuelle = cheminImageActuelle;
    }
}