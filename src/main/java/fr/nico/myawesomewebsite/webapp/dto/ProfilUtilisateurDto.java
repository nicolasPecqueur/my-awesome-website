package fr.nico.myawesomewebsite.webapp.dto;

import java.util.Calendar;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

import fr.nico.myawesomewebsite.domaine.referentiel.SexeEnum;

public class ProfilUtilisateurDto {
    
    public ProfilUtilisateurDto() {
        this.anneeDeNaissance = Calendar.getInstance().get(Calendar.YEAR) - 18;
    }

    private String prenom;
    private String nom;
    private SexeEnum sexe;
    private int anneeDeNaissance;
    @Email
    private String mail;
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$")
    private String motDePasse;

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public SexeEnum getSexe() {
        return this.sexe;
    }

    public void setSexe(SexeEnum sexe) {
        this.sexe = sexe;
    }

    public int getAnneeDeNaissance() {
        return this.anneeDeNaissance;
    }

    public void setAnneeDeNaissance(int anneeDeNaissance) {
        this.anneeDeNaissance = anneeDeNaissance;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMotDePasse() {
        return this.motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }
}
