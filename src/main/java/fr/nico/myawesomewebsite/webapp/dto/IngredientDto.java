package fr.nico.myawesomewebsite.webapp.dto;

import java.io.Serializable;

/**
 * The Class IngredientDto.
 */
public class IngredientDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6479470939043540430L;

	/** The id. */
	private long id;
	
	/** The code barre. */
	private String codeBarre;
	
	/** The nom. */
	private String nom;
	
	/** The proteines. */
	private double proteines;
	
	/** The glucides. */
	private double glucides;
	
	/** The lipides. */
	private double lipides;
	
	/** The energie. */
	private int energie;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Gets the code barre.
	 *
	 * @return the code barre
	 */
	public String getCodeBarre() {
		return codeBarre;
	}
	
	/**
	 * Sets the code barre.
	 *
	 * @param codeBarre the new code barre
	 */
	public void setCodeBarre(String codeBarre) {
		this.codeBarre = codeBarre;
	}

	/**
	 * Gets the nom.
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 *
	 * @param nom the new nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Gets the proteines.
	 *
	 * @return the proteines
	 */
	public double getProteines() {
		return proteines;
	}

	/**
	 * Sets the proteines.
	 *
	 * @param proteines the new proteines
	 */
	public void setProteines(double proteines) {
		this.proteines = proteines;
	}

	/**
	 * Gets the glucides.
	 *
	 * @return the glucides
	 */
	public double getGlucides() {
		return glucides;
	}

	/**
	 * Sets the glucides.
	 *
	 * @param glucides the new glucides
	 */
	public void setGlucides(double glucides) {
		this.glucides = glucides;
	}

	/**
	 * Gets the lipides.
	 *
	 * @return the lipides
	 */
	public double getLipides() {
		return lipides;
	}

	/**
	 * Sets the lipides.
	 *
	 * @param lipides the new lipides
	 */
	public void setLipides(double lipides) {
		this.lipides = lipides;
	}

	/**
	 * Gets the energie.
	 *
	 * @return the energie
	 */
	public int getEnergie() {
		return energie;
	}

	/**
	 * Sets the energie.
	 *
	 * @param energie the new energie
	 */
	public void setEnergie(int energie) {
		this.energie = energie;
	}

}
