package fr.nico.myawesomewebsite.webapp.dto.fil;

public class FilForPageDto extends FilDto {

    private boolean appercu = true;

    public boolean isAppercu() {
        return this.appercu;
    }

    public void setAppercu(boolean appercu) {
        this.appercu = appercu;
    }

}