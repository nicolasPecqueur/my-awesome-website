package fr.nico.myawesomewebsite.webapp.dto;

import java.util.ArrayList;
import java.util.List;

import fr.nico.myawesomewebsite.domaine.referentiel.ProfilEnum;
import fr.nico.myawesomewebsite.domaine.referentiel.SexeEnum;

/**
 * The Class UtilisateurDTO.
 */
public class UtilisateurAgeDTO {
    
    /** The id. */
    private Integer id;
    
    /** The mail. */
    private String mail;
    
    /** The mot de passe. */
    private String motDePasse;
    
    /** The nom. */
    private String nom;
    
    /** The prenom. */
    private String prenom;
    
    /** The age. */
    private int age;
    
    /** The sexe. */
    private SexeEnum sexe;
    
    /** The profil. */
    private ProfilEnum profil;
    
    /** The gabarit dto. */
    private GabaritDto gabaritDto;
    
    /** The metabolismes dto. */
    private List<MetabolismeDto> metabolismesDto = new ArrayList<>();
    
    /** The rythme dto. */
    private RythmeDto rythmeDto;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the mail.
	 *
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * Sets the mail.
	 *
	 * @param mail the new mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * Gets the mot de passe.
	 *
	 * @return the mot de passe
	 */
	public String getMotDePasse() {
		return motDePasse;
	}

	/**
	 * Sets the mot de passe.
	 *
	 * @param motDePasse the new mot de passe
	 */
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	/**
	 * Gets the nom.
	 *
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 *
	 * @param nom the new nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Gets the prenom.
	 *
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Sets the prenom.
	 *
	 * @param prenom the new prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Gets the age.
	 *
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Sets the age.
	 *
	 * @param age the new age
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * Gets the sexe.
	 *
	 * @return the sexe
	 */
	public SexeEnum getSexe() {
		return sexe;
	}

	/**
	 * Sets the sexe.
	 *
	 * @param sexe the new sexe
	 */
	public void setSexe(SexeEnum sexe) {
		this.sexe = sexe;
	}

	/**
	 * Gets the profil.
	 *
	 * @return the profil
	 */
	public ProfilEnum getProfil() {
		return profil;
	}

	/**
	 * Sets the profil.
	 *
	 * @param profil the new profil
	 */
	public void setProfil(ProfilEnum profil) {
		this.profil = profil;
	}

	/**
	 * Gets the gabarit dto.
	 *
	 * @return the gabarit dto
	 */
	public GabaritDto getGabaritDto() {
		return gabaritDto;
	}

	/**
	 * Sets the gabarit dto.
	 *
	 * @param gabaritDto the new gabarit dto
	 */
	public void setGabaritDto(GabaritDto gabaritDto) {
		this.gabaritDto = gabaritDto;
	}

	/**
	 * Gets the metabolismes dto.
	 *
	 * @return the metabolismes dto
	 */
	public List<MetabolismeDto> getMetabolismesDto() {
		return metabolismesDto;
	}

	/**
	 * Sets the metabolismes dto.
	 *
	 * @param metabolismesDto the new metabolismes dto
	 */
	public void setMetabolismesDto(List<MetabolismeDto> metabolismesDto) {
		this.metabolismesDto = metabolismesDto;
	}

	/**
	 * Gets the rythme dto.
	 *
	 * @return the rythme dto
	 */
	public RythmeDto getRythmeDto() {
		return rythmeDto;
	}

	/**
	 * Sets the rythme dto.
	 *
	 * @param rythmeDto the new rythme dto
	 */
	public void setRythmeDto(RythmeDto rythmeDto) {
		this.rythmeDto = rythmeDto;
	}
    
}
