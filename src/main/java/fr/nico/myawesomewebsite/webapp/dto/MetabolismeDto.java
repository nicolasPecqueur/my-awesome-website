package fr.nico.myawesomewebsite.webapp.dto;

import fr.nico.myawesomewebsite.domaine.referentiel.TypeMetabolismeEnum;

/**
 * The Class MetabolismeDto.
 */
public class MetabolismeDto {

    /** The id. */
    private Long id;
    
    /** The valeur. */
    private int valeur;
    
    /** The type. */
    private TypeMetabolismeEnum type;
    
    /** The proteines repas. */
    private double proteinesRepas;
    
    /** The lipides repas. */
    private double lipidesRepas;
    
    /** The glucides repas. */
    private double glucidesRepas;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the valeur.
	 *
	 * @return the valeur
	 */
	public int getValeur() {
		return valeur;
	}

	/**
	 * Sets the valeur.
	 *
	 * @param valeur the new valeur
	 */
	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public TypeMetabolismeEnum getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(TypeMetabolismeEnum type) {
		this.type = type;
	}

	/**
	 * Gets the proteines repas.
	 *
	 * @return the proteines repas
	 */
	public double getProteinesRepas() {
		return proteinesRepas;
	}

	/**
	 * Sets the proteines repas.
	 *
	 * @param proteinesRepas the new proteines repas
	 */
	public void setProteinesRepas(double proteinesRepas) {
		this.proteinesRepas = proteinesRepas;
	}

	/**
	 * Gets the lipides repas.
	 *
	 * @return the lipides repas
	 */
	public double getLipidesRepas() {
		return lipidesRepas;
	}

	/**
	 * Sets the lipides repas.
	 *
	 * @param lipidesRepas the new lipides repas
	 */
	public void setLipidesRepas(double lipidesRepas) {
		this.lipidesRepas = lipidesRepas;
	}

	/**
	 * Gets the glucides repas.
	 *
	 * @return the glucides repas
	 */
	public double getGlucidesRepas() {
		return glucidesRepas;
	}

	/**
	 * Sets the glucides repas.
	 *
	 * @param glucidesRepas the new glucides repas
	 */
	public void setGlucidesRepas(double glucidesRepas) {
		this.glucidesRepas = glucidesRepas;
	}
}
