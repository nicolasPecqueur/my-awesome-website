package fr.nico.myawesomewebsite.webapp.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;
import fr.nico.myawesomewebsite.persistance.UtilisateurRepository;

@Configuration
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {

        Utilisateur utilisateur = this.utilisateurRepository.findByMail(username).orElse(null);
        if (utilisateur == null) {
            throw new UsernameNotFoundException(username);
        }

        return User
                .withUsername(utilisateur.getMail())
                .password(utilisateur.getMotDePasse())
                .roles(utilisateur.getProfil().toString())
                .build();
    }
}
