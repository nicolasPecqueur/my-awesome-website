package fr.nico.myawesomewebsite.webapp.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// @formatter:off

/**
 * The Class SpringSecurityConfiguration.
 */
@Configuration
@EnableWebSecurity
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    /** The Constant HOME. */
    private static final String HOME = "/";

    /**
     * Configure.
     *
     * @param httpSecurity the http security
     * @throws Exception the exception
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        this.freeAccessFor(HOME + "/error", httpSecurity);
        this.freeAccessFor("/connexion", httpSecurity);
        this.freeAccessFor("/enregistrer", httpSecurity);
        this.freeAccessFor("/fil/page-principale/**", httpSecurity);
        this.freeAccessFor("/fil/consulter/**", httpSecurity);
        this.freeAccessFor("/fil/rubrique/**", httpSecurity);
        this.freeAccessFor("/fil/mur-tags/**", httpSecurity);
        this.freeAccessFor("/annuaire/**", httpSecurity);
        this.freeAccessFor("/webjars/**", httpSecurity);
        this.freeAccessForLogin(httpSecurity);
        this.freeAccessForLogOut(httpSecurity);
        this.mandatoryAuthenticationForEverythingElse(httpSecurity);
    }

    /**
     * Free access for.
     *
     * @param endPoint the end point
     * @param httpSecurity the http security
     * @throws Exception the exception
     */
    private void freeAccessFor(String endPoint, HttpSecurity httpSecurity) throws Exception {
        httpSecurity
        .authorizeRequests()
        .antMatchers(endPoint)
        .permitAll();
    }

    /**
     * Free access for login.
     *
     * @param httpSecurity the http security
     * @throws Exception the exception
     */
    private void freeAccessForLogin(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
        .formLogin()
        .loginPage("/connexion")
        .permitAll()
        .failureUrl("/loginFailure")
        .successForwardUrl(HOME)
        .defaultSuccessUrl(HOME);
    }

    /**
     * Mandatory authentication for everything else.
     *
     * @param httpSecurity the http security
     * @throws Exception the exception
     */
    private void mandatoryAuthenticationForEverythingElse(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
        .authorizeRequests()
        .anyRequest()
        .authenticated()
        .and()
        .formLogin();
    }

    /**
     * Free access for log out.
     *
     * @param httpSecurity the http security
     * @throws Exception the exception
     */
    private void freeAccessForLogOut(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
        .logout()
        .permitAll()
        .logoutSuccessUrl(HOME);
    }

    /**
     * Configure.
     *
     * @param web the web
     * @throws Exception the exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        this.ignoreSecurityIfResource(web, "css");
        this.ignoreSecurityIfResource(web, "js");
        this.ignoreSecurityIfResource(web, "img");
        this.ignoreSecurityIfResource(web, "files");
        this.ignoreSecurityIfResource(web, "fonts");
    }

    /**
     * Ignore security if resource.
     *
     * @param web the web
     * @param resource the resource
     */
    private void ignoreSecurityIfResource(WebSecurity web, String resource) {
        web
        .ignoring()
        .antMatchers("/" + resource + "/**");
    }
}

// @formatter:on