package fr.nico.myawesomewebsite.webapp.mapping.impl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import fr.nico.myawesomewebsite.domaine.entities.Metabolisme;
import fr.nico.myawesomewebsite.webapp.dto.MetabolismeDto;
import fr.nico.myawesomewebsite.webapp.mapping.MetabolismeMapper;

/**
 * The Class MetabolismeMapperImpl.
 */
@Component
public class MetabolismeMapperImpl implements MetabolismeMapper {

    /**
     * Metabolisme to metabolisme dto.
     *
     * @param source the source
     * @return the metabolisme dto
     */
    @Override
    public MetabolismeDto metabolismeToMetabolismeDto(Metabolisme source) {
        
    	if (source == null) {
    		return null;
    	}
    	
        MetabolismeDto target = new MetabolismeDto();
        target.setId(source.getId());
        target.setValeur(source.getValeur());
        target.setType(source.getType());
        target.setProteinesRepas(source.getProteinesRepas());
        target.setLipidesRepas(source.getLipidesRepas());
        target.setGlucidesRepas(source.getGlucidesRepas());
        
        return target;
    }

    /**
     * Metabolisme to metabolisme dto.
     *
     * @param source the source
     * @return the list
     */
    @Override
    public List<MetabolismeDto> metabolismeToMetabolismeDto(Set<Metabolisme> source) {
        
        List<MetabolismeDto> target = new ArrayList<>();
        
        for ( Metabolisme e : source) {
            target.add(this.metabolismeToMetabolismeDto(e));
        }
        
        return target;
    }

    /**
     * Metabolisme to metabolisme dto.
     *
     * @param source the source
     * @return the list
     */
    @Override
    public List<MetabolismeDto> metabolismeToMetabolismeDto(List<Metabolisme> source) {

        List<MetabolismeDto> target = new ArrayList<>();
        
        for ( Metabolisme e : source) {
            target.add(this.metabolismeToMetabolismeDto(e));
        }
        
        return target;
    }

    /**
     * Metabolisme dto to metabolisme.
     *
     * @param source the source
     * @return the metabolisme
     */
    @Override
    public Metabolisme metabolismeDtoToMetabolisme(MetabolismeDto source) {
        
        Metabolisme target = new Metabolisme();
        
        target.setId(source.getId());
        target.setType(source.getType());
        target.setValeur(source.getValeur());
        target.setProteinesRepas(source.getProteinesRepas());
        target.setLipidesRepas(source.getLipidesRepas());
        target.setGlucidesRepas(source.getGlucidesRepas());
        
        return target;
    }

    /**
     * Metabolisme dto to metabolisme.
     *
     * @param source the source
     * @return the sets the
     */
    @Override
    public Set<Metabolisme> metabolismeDtoToMetabolisme(List<MetabolismeDto> source) {
        
        Set<Metabolisme> target = new LinkedHashSet<>();
        for (MetabolismeDto mDto : source) {
            target.add(this.metabolismeDtoToMetabolisme(mDto));
        }
        
        return target;
    }

}
