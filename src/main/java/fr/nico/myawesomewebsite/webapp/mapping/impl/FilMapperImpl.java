package fr.nico.myawesomewebsite.webapp.mapping.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import fr.nico.myawesomewebsite.domaine.entities.Fil;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilForPageDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilForRubriqueDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilResumeDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FilToUpdateDto;
import fr.nico.myawesomewebsite.webapp.dto.fil.FortuneDto;
import fr.nico.myawesomewebsite.webapp.mapping.FilMapper;
import fr.nico.myawesomewebsite.webapp.mapping.TagMapper;

@Component
public class FilMapperImpl implements FilMapper {

    @Value("${file.upload.resource.name}")
    private String fileUploadResourceName;

    @Autowired
    private TagMapper tagMapper;

    @Override
    public FilDto filToFilDto(Fil source) {
        if (source == null) {
            return null;
        }
        return this.filToFilForPageDto(source);
    }

    @Override
    public Page<FilForPageDto> filsToPageOfFils(Page<Fil> source) {
        List<FilForPageDto> target = this.filToFilForPageDto(source.getContent());
        return new PageImpl<>(target, source.getPageable(), source.getTotalElements());
    }

    @Override
    public List<FilForPageDto> filToFilForPageDto(List<Fil> source) {
        List<FilForPageDto> target = new ArrayList<>();
        for (Fil fil : source) {
            target.add(this.filToFilForPageDto(fil));
        }
        return target;
    }

    @Override
    public FilForPageDto filToFilForPageDto(Fil source) {
        FilForPageDto target = new FilForPageDto();
        target.setId(source.getId());
        target.setTitre(source.getTitre());
        target.setMessage(source.getHtmlMessage());
        target.setLien(source.getLien());
        target.setDateCreation(source.getDateCreation());
        target.setDateMiseAJour(source.getDateMiseAJour());
        if (source.getAuteur() != null) {
            target.setAuteurId(source.getAuteur().getId());
            target.setAuteurNom(source.getAuteur().getNom());
            target.setAuteurPrenom(source.getAuteur().getPrenom());
        }
        if ((source.getNomImage() != null) && (source.getAuteur() != null)) {
            target.setImage(this.getImagePath(source));
        }
        target.setAppercu(source.isAppercu());
        target.setTags(this.tagMapper.tagToTagDto(source.getTags()));
        return target;
    }

    private String getImagePath(Fil source) {
        LocalDateTime dateCreation = source.getDateCreation();
        return File.separator 
                + this.fileUploadResourceName 
                + File.separator + source.getAuteur().getId() 
                + File.separator + dateCreation.getYear()
                + File.separator + (dateCreation.getMonthValue()) 
                + File.separator 
                + source.getNomImage();
    }

    @Override
    public FilToUpdateDto filToFilToUpdateDto(Fil source) {
        if ((source == null) || (source.getAuteur() == null)) {
            return null;
        }
        FilToUpdateDto target = new FilToUpdateDto();
        target.setTitre(source.getTitre());
        target.setMessage(source.getMessage());
        target.setLien(source.getLien());
        if ((source.getNomImage() != null) && (source.getAuteur() != null)) {
            target.setCheminImageActuelle(this.getImagePath(source));
        }
        target.setAppercu(source.isAppercu());
        return target;
    }

    @Override
    public FilResumeDto filToFilResumeDto(Fil source) {
        if (source == null) {
            return null;
        }
        FilResumeDto target = new FilResumeDto();
        target.setId(source.getId());
        target.setTitre(source.getTitre());
        return target;
    }

    @Override
    public FilForRubriqueDto filToFilForRubriqueDto(Fil source) {
        if (source == null) {
            return null;
        }
        FilForRubriqueDto target = new FilForRubriqueDto();
        target.setId(source.getId());
        target.setTitre(source.getTitre());
        if (source.getAuteur() != null) {
            target.setAuteur(source.getAuteur().getPrenom() + " " + source.getAuteur().getNom());
        }
        if ((source.getNomImage() != null) && (source.getAuteur() != null)) {
            target.setImage(this.getImagePath(source));
        }
        target.setDateCreation(source.getDateCreation());
        target.setDateMiseAJour(source.getDateMiseAJour());
        return target;
    }

    @Override
    public List<FilForRubriqueDto> filToFilForRubriqueDto(List<Fil> source) {
        if (source.isEmpty()) {
            return Collections.emptyList();
        }
        List<FilForRubriqueDto> target = new ArrayList<>();
        for (Fil fil : source) {
            target.add(this.filToFilForRubriqueDto(fil));
        }
        return target;
    }

    @Override
    public Page<FilForRubriqueDto> filToFilForRubriqueDto(Page<Fil> source) {
        List<FilForRubriqueDto> target = this.filToFilForRubriqueDto(source.getContent());
        return new PageImpl<>(target, source.getPageable(), source.getTotalElements());
    }

    @Override
    public FortuneDto filToFortuneDto(Fil source) {
        FortuneDto target = new FortuneDto();
        target.setLibelle(source.getMessage().split(" -- ")[0]);
        target.setAuteur(source.getMessage().split(" -- ")[1]);   
        return target;
    }
}