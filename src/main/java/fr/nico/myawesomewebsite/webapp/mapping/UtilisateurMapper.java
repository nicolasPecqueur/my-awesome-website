package fr.nico.myawesomewebsite.webapp.mapping;

import java.util.List;

import fr.nico.myawesomewebsite.domaine.entities.Utilisateur;
import fr.nico.myawesomewebsite.webapp.dto.ProfilUtilisateurDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurAgeDTO;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;

public interface UtilisateurMapper {

    /**
     * Nouvel utilisateur dto to utilisateur.
     *
     * @param nouvelUtilisateurDto the nouvel utilisateur dto
     * @return the utilisateur
     */
    Utilisateur nouvelUtilisateurDtoToUtilisateur(ProfilUtilisateurDto nouvelUtilisateurDto);

    ProfilUtilisateurDto utilisateurToProfilUtilisateurDto(Utilisateur utilisateur);

    /**
     * Utilisateur to UtilisateurDto.
     *
     * @param utilisateur the utilisateur
     * @return the utilisateur DTO
     */
    UtilisateurDto utilisateurToUtilisateurDto( Utilisateur utilisateur );

    /**
     * To entity.
     *
     * @param utilisateurDTO the utilisateur DTO
     * @return the utilisateur
     */
    Utilisateur utilisateurDtoToUtilisateur( UtilisateurDto utilisateurDTO );

    /**
     * Utilisateur to utilisateur dto.
     *
     * @param uListe the u liste
     * @return the list
     */
    List<UtilisateurDto> utilisateurToUtilisateurDto( Iterable<Utilisateur> uListe );

    /**
     * To dto.
     *
     * @param uListe the u liste
     * @return the list
     */
    List<UtilisateurDto> toDto(List<Utilisateur> uListe);

    /**
     * Utilisateur dto to utilisateur age dto.
     *
     * @param utilisateurDto the utilisateur dto
     * @return the utilisateur age DTO
     */
    UtilisateurAgeDTO utilisateurDtoToUtilisateurAgeDto(UtilisateurDto utilisateurDto);

    /**
     * Mise A jour utilisateur.
     *
     * @param utilisateurToSave the utilisateur to save
     * @param utilisateurBdd the utilisateur bdd
     * @return the utilisateur
     */
    Utilisateur miseAJourUtilisateur(Utilisateur utilisateurToSave, Utilisateur utilisateurBdd);
}
