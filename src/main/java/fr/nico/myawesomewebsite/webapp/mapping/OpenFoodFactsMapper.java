package fr.nico.myawesomewebsite.webapp.mapping;

import fr.nico.myawesomewebsite.domaine.entities.Ingredient;
import fr.nico.myawesomewebsite.webapp.dto.IngredientDto;
import fr.nico.myawesomewebsite.webapp.dto.openfoodfacts.OpenFoodFactsResponseDto;

/**
 * The Interface OpenFoodFactsMapper.
 */
public interface OpenFoodFactsMapper {

	/**
	 * Open food facts response dto to aliment dto.
	 *
	 * @param source the source
	 * @param codeBarre 
	 * @return the ingredient dto
	 */
	IngredientDto openFoodFactsResponseDtoToAlimentDto(OpenFoodFactsResponseDto source, String codeBarre);
	
	/**
	 * Ingredient to ingredient dto.
	 *
	 * @param source the source
	 * @return the ingredient dto
	 */
	IngredientDto ingredientToIngredientDto(Ingredient source);
	
	/**
	 * Ingredient dto to ingredient.
	 *
	 * @param source the source
	 * @return the ingredient
	 */
	Ingredient ingredientDtoToIngredient(IngredientDto source);
}
