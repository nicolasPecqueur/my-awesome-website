package fr.nico.myawesomewebsite.metier;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    public boolean isAdminstrateur(Authentication auth) {
        GrantedAuthority adminAuthority = new SimpleGrantedAuthority("ROLE_Administrateur");
        return auth.getAuthorities().contains(adminAuthority);
    }

}
