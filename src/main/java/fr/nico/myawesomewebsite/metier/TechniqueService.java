package fr.nico.myawesomewebsite.metier;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * The Class TechniqueService.
 */
@Service
public class TechniqueService {

    /** The Constant DOT_DELIMITER. */
    private static final String DOT_DELIMITER = ".";

    /** The root package name. */
    @Value("${project.root.package}")
    private String rootPackageName;

    /**
     * Gets the fields list.
     *
     * @param rootClass the root class
     * @return the fields list
     */
    public List<String> getFieldsList(Class<?> rootClass) {
        List<String> reccursiveFieldsList = new ArrayList<>();
        String[] rootClassRootPackageName = StringUtils.split(rootClass.getPackageName(), DOT_DELIMITER);
        if ((rootClassRootPackageName == null) || !rootClassRootPackageName[0].equals(this.rootPackageName)) {
            return Collections.emptyList();
        }
        this.getRecursiveFieldsList(rootClass, reccursiveFieldsList, "");
        return reccursiveFieldsList;
    }

    /**
     * Gets the recursive fields list.
     *
     * @param classToAnalyse the class to analyse
     * @param fieldsList the fields list
     * @param fieldsPath the fields path
     * @return the recursive fields list
     */
    private void getRecursiveFieldsList(Class<?> classToAnalyse, List<String> fieldsList, String fieldsPath) {
        String initialFieldPath = fieldsPath;
        Field[] fieldsArray = classToAnalyse.getDeclaredFields();
        for (Field field : fieldsArray) {
            if (field.getType().isAnnotationPresent(Entity.class)) {
                fieldsPath = !fieldsPath.isBlank() ? fieldsPath.concat(DOT_DELIMITER + field.getName()) : field.getName();
                this.getRecursiveFieldsList(field.getType(), fieldsList, fieldsPath);
            } else {
                if (!fieldsPath.isBlank()) {
                    fieldsList.add(fieldsPath.concat(DOT_DELIMITER).concat(field.getName()));
                } else {
                    fieldsList.add(field.getName());
                }
                if (field.getType().isPrimitive() || field.getType().equals(String.class) || field.getType().equals(Date.class)
                        || field.getType().equals(Long.class)) {
                    System.err.println();
                }
            }
            fieldsPath = initialFieldPath;
        }
    }
    
    /**
     * Gets the os.
     *
     * @return the os
     */
    public String getOs() {
        return System.getProperty("os.name").toLowerCase();
    }

    /**
     * Checks if is linux.
     *
     * @return true, if is linux
     */
    public boolean isLinux() {
        return getOs().startsWith("linux");
    }
}
