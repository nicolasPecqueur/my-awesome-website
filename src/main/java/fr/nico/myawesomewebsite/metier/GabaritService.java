package fr.nico.myawesomewebsite.metier;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.nico.myawesomewebsite.domaine.entities.Gabarit;
import fr.nico.myawesomewebsite.domaine.exception.RegleGestionException;
import fr.nico.myawesomewebsite.domaine.referentiel.SexeEnum;
import fr.nico.myawesomewebsite.persistance.GabaritRepository;
import fr.nico.myawesomewebsite.webapp.dto.GabaritDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;
import fr.nico.myawesomewebsite.webapp.mapping.GabaritMapper;

/**
 * The Class GabaritService.
 */
@Service
@Transactional
public class GabaritService {

    /** The gabarit repository. */
    @Autowired
    private GabaritRepository gabaritRepository;
    
    /** The gabarit mapper. */
    @Autowired
    private GabaritMapper gabaritMapper;

    /** The utilisateur service. */
    @Autowired
    private UtilisateurService utilisateurService;

    /**
     * Donner gabarit par defaut pout un homme ou une femme
     * @param utilisateurDto 
     *
     * @return the gabarit dto
     */
    public GabaritDto donnerGabaritParDefaut(UtilisateurDto utilisateurDto) {
    	
    	GabaritDto dto = new GabaritDto();
    	if (utilisateurDto.getSexe() == SexeEnum.F) {
    		dto.setPoids(55);
    		dto.setTaille(160);
    		dto.setTourDeCou(20.0);
    		dto.setTourDeTaille(60.0);
    	} else {
    		dto.setPoids(75);
    		dto.setTaille(180);
    		dto.setTourDeCou(40);
    		dto.setTourDeTaille(75);    		
    	}

        return dto;
    }

    /**
     * Initialiser premier gabarit.
     *
     * @param utilisateurDto the utilisateur dto
     * @return the utilisateur DTO
     */
    public UtilisateurDto initialiserPremierGabarit(UtilisateurDto utilisateurDto) {
        
        GabaritDto gabaritDtoParDefaut = this.donnerGabaritParDefaut(utilisateurDto);
        gabaritDtoParDefaut = this.enregistrerGabarit(gabaritDtoParDefaut);
        utilisateurDto.setGabaritDto(gabaritDtoParDefaut);
        utilisateurDto = this.utilisateurService.enregistrerUtilisateur(utilisateurDto);
        
        return utilisateurDto;        
    }

    /**
     * Enregistrer gabarit.
     *
     * @param gabaritDto the gabarit dto
     * @return the gabarit dto
     */
    public GabaritDto enregistrerGabarit(GabaritDto gabaritDto) {
        
        Gabarit gabaritToSave = this.gabaritMapper.gabaritDtoToGabarit(gabaritDto);
        gabaritToSave = this.gabaritRepository.save(gabaritToSave);
        
        return this.gabaritMapper.gabaritToGabaritDto(gabaritToSave);
    }
    
    /**
     * Rechercher par utilisateur.
     *
     * @param utilisateurDto the utilisateur dto
     * @return the gabarit dto
     */
    public GabaritDto rechercherParUtilisateur(UtilisateurDto utilisateurDto) {
    	
    	Gabarit gabaritBdd = this.gabaritRepository.findByUser(utilisateurDto.getId()).orElse(null);
    	
    	if (gabaritBdd == null) {
    		throw new RegleGestionException("Aucun gabarit n'a été trouvé en base de donnée pour cet utilisateur!");
    	}
    	
    	return this.gabaritMapper.gabaritToGabaritDto(gabaritBdd);
    }
}
