package fr.nico.myawesomewebsite.metier;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.nico.myawesomewebsite.domaine.entities.Rythme;
import fr.nico.myawesomewebsite.domaine.referentiel.RythmeEntrainementHebdomadaireEnum;
import fr.nico.myawesomewebsite.persistance.RythmeRepository;
import fr.nico.myawesomewebsite.webapp.dto.RythmeDto;
import fr.nico.myawesomewebsite.webapp.dto.UtilisateurDto;
import fr.nico.myawesomewebsite.webapp.mapping.RythmeMapper;

/**
 * The Class RythmeService.
 */
@Service
@Transactional
public class RythmeService {
	
	/** The rythme repository. */
	@Autowired
	private RythmeRepository rythmeRepository;
	
	/** The rythme mapper. */
	@Autowired
	private RythmeMapper rythmeMapper;
	
	/** The nb repas par jourdefaut. */
	@Value("${rythme.default.nombre.repas.journalier}")
	private int nbRepasParJourdefaut;
	
	/** The proteine par kilodefaut. */
	@Value("${rythme.default.proteine.par.kilo}")
	private double proteineParKilodefaut;

	/** The lipide par kilodefaut. */
	@Value("${rythme.default.lipide.par.kilo}")
	private double lipideParKilodefaut;

	/**
	 * Initialiser tout.
	 *
	 * @return the rythme dto
	 */
	public RythmeDto donnerRythmeParDefaut() {
		
		Rythme rythmeToSave = new Rythme();
		
		// aucun entrainement par défaut
		rythmeToSave.setEntrainementHebdomadaire(RythmeEntrainementHebdomadaireEnum.ZERO);
		
		// rythme alimentaire
		rythmeToSave.setNbRepasJour(nbRepasParJourdefaut);
		rythmeToSave.setProteineKilo(proteineParKilodefaut);
		rythmeToSave.setLipideKilo(lipideParKilodefaut);
		
		// persistance
		rythmeToSave = this.rythmeRepository.save(rythmeToSave);
		
		// fin
		return this.rythmeMapper.rythmeToRythmeDto(rythmeToSave);
	}

	/**
	 * Enregistrer rythme.
	 *
	 * @param rythmeDto the rythme dto
	 * @return the rythme dto
	 */
	public RythmeDto enregistrerRythme(RythmeDto rythmeDto) {
		
		Rythme rythmeToSave = this.rythmeMapper.rythmeDtoToRythme(rythmeDto);
		rythmeToSave = this.rythmeRepository.save(rythmeToSave);
		return this.rythmeMapper.rythmeToRythmeDto(rythmeToSave);
	}
	
	/**
	 * Rechercher rythme par utilisateur.
	 *
	 * @param utilisateurDto the utilisateur dto
	 * @return the rythme dto
	 */
	public RythmeDto rechercherRythmeParUtilisateur(UtilisateurDto utilisateurDto) {
		
		Rythme rythmeBdd = this.rythmeRepository.getRythmeByUserId(utilisateurDto.getId()).orElse(null);
		
		if (rythmeBdd == null) {
			return null;
		}
		
		return this.rythmeMapper.rythmeToRythmeDto(rythmeBdd);
	}

}
