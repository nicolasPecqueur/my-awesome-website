function preventDefault(event) {
	if (event.preventDefault) {
		event.preventDefault();
	} else {
		event.returnValue = false;
	}
}

function forEachAddEventListener(collection, event, func) {
	collection.forEach( (c) => c.addEventListener( event, func ));
}

function forEachInArrayAddEventListener(array, event, func) {
	for(let i = 0 ; i < array.length ; i++)
	{
	    array[i].addEventListener( event, func );
	    console.log(array[i]);
	}
}

var alertList = document.querySelectorAll('.alert')
var alerts =  [].slice.call(alertList).map(function (element) {
  return new bootstrap.Alert(element)
})

function initClassForParagraphWithImg() {
	let img2 = document.getElementsByTagName("img");
	if (img2.length > 0) {
		for (let k = 0 ; k < img2.length ; ++k ) {
			if (img2[k].getAttribute('class') == 'in-article') {
				img2[k].closest("p").setAttribute('class', 'with-img');
			}
		}
	}
}

window.addEventListener( "load" , initClassForParagraphWithImg );